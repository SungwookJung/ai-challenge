# AI-challenge


    F330 사이즈 기체

    tx2 + auvidea J120 

        pixhawk2 or pixFalcon (소형)
            px4flow (하방)
            sonar or Lidar lite (좌,우, 하)
    
        ZED or ZED mini

            $ sudo gedit /usr/local/bin/disableUSBAutosuspend.sh  
            --> L4T는 기본적으로 에너지 절약모드로 동작하도록 되어 있는데 특히 USB 포트의 경우에는 사용을 하지 않으면 대기모드로 빠지게 된다. 이렇게 되면 연결되어 있는 센서들과의 통신이 제대로 이루어지지 않으므로 autosuspend를 막는 스크립트를 추가하도록 한다.
            파일이 열리면 아래의 내용을 추가후 저장하고 닫는다.

                        #!/bin/sh
                        sudo sh -c 'for dev in /sys/bus/usb/devices/*/power/autosuspend; do echo -1 >$dev; done'

            ZED 스테레오 카메라의 성능을 최대로 끌어내기 위해서 ZED SDK에서 제공하는 스크립트를 부팅때 자동으로 실행시킨다. 추가로 위에서 작성한 쉘스크립트를 실행하도록 하는 내용도 추가하자. 편집기로 /etc/rc.local 파일을 열고 다음과 같이 입력을 하면 된다. (파일을 열어서 제일 하단의 'exit 0' 바로 위에 입력한다)

            # Turn up the CPU and GPU for max performance
            /usr/local/zed/scripts/jetson_max_l4t_updated.sh

            # disable USB autosuspend
            /usr/local/bin/disableUSBAutosuspend.sh

            수정한 파일에 실행권한을 부여하자.
            $ sudo chmod +x /etc/rc.local



pixhawk2 

px4flow -> optical flow

zed stereo -> vi-o -> svo, vins mono

           -> octomap -> obstacle avoidance


1D lidar -> altitude estimation

각 테스크가 끝났다는 flag 어떻게??

rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB0 _baud:=500000
-> "ardu_ros" 단축 명령어로 만들어둠.  




    



