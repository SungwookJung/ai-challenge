QgroundControl 버전은 3.5V !!!!!!!!!!!!!!!


| Page          | Param         | Value         | 
| ------------- | ------------- |-------------  | 
| System        | SYS_MC_EST_GROUP  | EKF2         | 
| System  | SYS_COMPANION  |Companion Link(921600 baud, 8N1) (for jetson)         | 
| Mission        | MIS_TAKEOFF_ALT  | 1.5         | 
| Multicopter Position Control        | MPC_ALT_MODE  | % not sure, cur: altitude following %         | 
| Multicopter Position Control        | MPC_XY_P  | % not sure, cur: 0.95         | 
| Multicopter Position Control        | MPC_XY_VEL_MAX  | 2         | 
| Sensor Enable	        | SENS_EN_LL40LS  | PWM         | 
| EKF2        | EKF2_AID_MASK  | 26        | 
| EKF2        | EKF2_EV_DELAY  | 0         | 
| EKF2        | EKF2_HGT_MODE  | Vision    | 
| EKF2        | EKF2_MAG_NOISE  | NONE  % not sure       | 

